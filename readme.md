# Social Media Site

This is a social media site backend build on Node.js Express PostgreSQL and Prisma

## Installation

Run: 
```bash
npm i
npm run watch
npm run dev
```

## Usage

```bash
curl http://localhost:3000/api/user

```
```bash
curl http://localhost:3000/api/user/:id
```
```bash
curl http://localhost:3000/api/post
```
```bash
curl http://localhost:3000/api/post/:id
```
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)